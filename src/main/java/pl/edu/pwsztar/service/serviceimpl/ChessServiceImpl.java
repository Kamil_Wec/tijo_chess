package pl.edu.pwsztar.service.serviceimpl;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.service.MoveChessService;

import java.util.Arrays;

@Service
public class ChessServiceImpl implements MoveChessService {
    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto){
        if(figureMoveDto.getType() == FigureType.BISHOP) {
            int[] currentPosition = Arrays.stream(figureMoveDto.getStart().split("_"))
                    .mapToInt(position -> position.charAt(0))
                    .toArray();

            int[] destinationPosition = Arrays.stream(figureMoveDto.getDestination().split("_"))
                    .mapToInt(position -> position.charAt(0))
                    .toArray();

            return Math.abs((currentPosition[0] - destinationPosition[0])) == Math.abs((currentPosition[1] - destinationPosition[1]));
        }
        return false;
    }


}