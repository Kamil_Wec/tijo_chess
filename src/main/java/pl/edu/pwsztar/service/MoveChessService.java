package pl.edu.pwsztar.service;

import pl.edu.pwsztar.domain.dto.FigureMoveDto;

public interface MoveChessService {
    boolean isCorrectMove(FigureMoveDto figureMoveDto);
}